#!/bin/bash


########################################################################
#                                                                      #
#                          Q N A P S T A T S                           #
#                                                                      #
#            STATISTICS DATABASE FIX FOR QNAP APPLICATIONS             #
#           CLOUD BACKUP SYNC / HYBRID BACKUP SYNC / S3 PLUS           #
#                                                                      #
#                      Copyright 2018 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                       Version 1.0 / 28.01.2018                       #
#                                                                      #
########################################################################
#
# There are problems with the 'Cloud Backup Sync', 'Hybrid Backup Sync'
# or 'S3 Plus' applications of QNAP if you make your backup to 'Amazon
# Web Services' (AWS) and you are changing the configuration of your
# backup job. Maybe you want to add some folders to the backup or make
# some changes to the options. After saving the new configuration the
# backup will not work anymore and an error message is displayed:
#
#      "ERROR: "The Bucket is not empty. Please use a new bucket."
#
# The error messages can vary, depending on the software/version. In
# other words, you can never change an existing backup job without
# getting into troubles...
#
# To make the backup job run again you have to alter the 'statistics'
# database of the backup solution and enter a successful entry as the
# last one. After that the backup job should run again. This script
# will insert the necessary entry into the database without doing it
# manually with an SQL statement (which is also possible and will also
# work and get the backup job running again).
#
# ======================================================================
#
# Check that the 'sqlite3' software package is installed on your QNAP
# device. You can install the 'Entware-ng' package and later the the
# 'sqlite3' package with the included OPKG packet manager:
#
#  - Download the 'Entware-ng' package from:
#
#    https://github.com/Entware-ng/Entware-ng/wiki/Install-on-QNAP-NAS
#
#  - Install the 'Entware-ng' via the 'App Center' of your QNAP device.
#
#  - Connect to your QNAP device over a SSH connection.
#
#  - Update the OPKG package list - Command: opkg update
#
#  - Upgrade the existing OPKG packages - Command: opkg upgrade
#
#  - Install the 'sqlite3' package - Command: opkg install sqlite3
#
#  - Search for the folder where the 'statistics' database is located.
#
#    Usually: /share/CACHEDEV1_DATA/.qpkg/CloudBackupSync/data/0
#
#  - Copy the script 'qnapstats.sh' to the database folder.
#
#  - Make the script executable - Command: chmod +x /path/qnapstats.sh
#
#  - Execute the script to insert a successful backup entry into the
#    'statistics' database - Command: ./qnapstats.sh
#
#  - After that you can execute the 'Cloud Backup Sync' application and
#    the backup job should work again.
#
# ======================================================================


# ======================================================================
# C O N F I G U R A T I O N   -   P L E A S E   C U S T O M I Z E
# ======================================================================

#Specify the database name.
DATABASE="statistics"

# Specify the database directory.
#DATABASE_DIR="/share/CACHEDEV1_DATA/.qpkg/CloudBackupSync/data/0"
DATABASE_DIR="/home/neron/Temp/qnapstats"

# Specify the backup directory.
#BACKUP_DIR="/share/Web/backup"
BACKUP_DIR="./backup"

# Specify the output file.
OUTPUT="query_result.txt"

# Specify the actual date in the dd.mm.yyyy_hh-mm-ss format.
ACTUAL_DATE="$(date +"%d.%m.%Y_%H-%M-%S")"

# Specify the query to get the number of rows from the 'backup_job' table.
QUERY_1="SELECT count(*) FROM backup_job;"

# Specify the query to insert a successful backup into the database.
QUERY_2="INSERT INTO backup_job (job_id,state,sub_state,start_time,stop_time,number_of_files_to_process,number_of_files_scanned,size_to_process,size_scanned,number_of_files_transferred,size_transferred,number_of_files_failed,size_failed,deleted,estimated_completion_time,used_size_on_cloud) VALUES(1,'Success',NULL,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0,0,0,0,0,0,0,0,0,NULL,0);";

# Clear the screen.
clear

# Display the title.
echo "########################################################################"
echo "#                                                                      #"
echo "#             Q N A P S T A T S - D A T A B A S E   F I X              #"
echo "#                                                                      #"
echo "#                      Copyright 2018 by PB-Soft                       #"
echo "#                                                                      #"
echo "#                           www.pb-soft.com                            #"
echo "#                                                                      #"
echo "########################################################################"
echo

# Check if the database file exist.
if [ -f "$DATABASE_DIR"/"$DATABASE" ]; then

  # Display an information message.
  echo "The 'CloudBackupSync' database file '"$DATABASE"' was found..."

  # Check if the database backup directory does not exist.
  if [ ! -d "$BACKUP_DIR" ]; then

    # Display an information message.
    echo "Try to create the database backup directory..."

    # Create the database backup directory.
    mkdir -p "$BACKUP_DIR"

  fi # Check if the database backup directory does not exist.

  # Check if the database backup directory exist.
  if [ -d "$BACKUP_DIR" ]; then

    # Make a backup of the actual database.
    cp "$DATABASE_DIR"/"$DATABASE" "$BACKUP_DIR"/"$DATABASE"_"$ACTUAL_DATE"

    # Check if the database backup file exist.
    if [ -f "$BACKUP_DIR"/"$DATABASE"_"$ACTUAL_DATE" ]; then

      # Display an information message.
      echo "The database backup file '"$DATABASE"_"$ACTUAL_DATE"' was created..."

      # Execute the SQLite query 1.
      NUMBER_1="$(sqlite3 $DATABASE_DIR"/"$DATABASE "$QUERY_1")"

      # Display the number of existing backup jobs.
      echo
      echo "Number of listed backup jobs (before modification):" $NUMBER_1

      # Execute the SQLite query 2.
      sqlite3 $DATABASE_DIR"/"$DATABASE "$QUERY_2" > $DATABASE_DIR"/"$OUTPUT

      # Execute the SQLite query 1.
      NUMBER_2="$(sqlite3 $DATABASE_DIR"/"$DATABASE "$QUERY_1")"

      # Display the number of existing backup jobs.
      echo "Number of listed backup jobs (after modification):" $NUMBER_2

      # Check if the database modification was successful.
      if [ $NUMBER_2 -gt $NUMBER_1 ]; then

        # Display a success message.
        echo
        echo "The database modification was SUCCESSFUL!"
        echo "Please start your backup job again - It should work now!"

      # The the database modification was not successful.
      else

        # Display an error message.
        echo
        echo "ERROR: The database modification has FAILED!"
        echo "Please check that your database is unlocked and not in use and try again."

      fi # The the database modification was not successful.

    # The database backup file does not exist.
    else

      # Display an error message.
      echo "ERROR: The database backup file '"$DATABASE"_"$ACTUAL_DATE"' could not be created!"

    fi # The database backup file does not exist.

  # The database backup directory could not be created.
  else

    # Display an error message.
    echo "ERROR: The database backup directory could not be created!"

  fi # The database backup directory could not be created.

# The database file does not exist.
else

  # Display an error message.
  echo "ERROR: The database file '".$DATABASE_DIR"/"$DATABASE."' does not exist!"

fi # The database file does not exist.

# Add some space.
echo
