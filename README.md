# QNAPstats Script - README #
---

### Overview ###

The **QNAPstats** script (Bash) can be used to fix a problem with the backup applications 'Cloud Backup Sync', 'Hybrid Backup Sync' or 'S3 Plus' of QNAP. The script will insert a valid backup entry into the 'statistics' database and the backup jobs then should run again. More information is available in the header of the script.

### Screenshots ###

![QNAPstats - Statistics Database Fix](development/readme/qnapstats1.png "QNAPstats - Statistics Database Fix")

### Setup ###

* Download the **Entware-ng** package from the [**Github Website**](https://github.com/Entware-ng/Entware-ng/wiki/Install-on-QNAP-NAS).
* Install the **Entware-ng** via the **App Center** of your QNAP device.
* Connect to your QNAP device over a SSH connection.
* Update the OPKG package list - Command: opkg update
* Upgrade the existing OPKG packages - Command: opkg upgrade
* Install the **sqlite3** package - Command: opkg install sqlite3
* Search for the folder where the **statistics** database is located.
* Usually: /share/CACHEDEV1_DATA/.qpkg/CloudBackupSync/data/0
* Copy the script **qnapstats.sh** to the database folder.
* Make the script executable - Command: chmod +x /path/qnapstats.sh
* Execute the script to insert a successful backup entry into the **statistics** database - Command: ./qnapstats.sh
* After that you can execute the **Cloud Backup Sync** application and the backup job should work again.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **QNAPstats** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
